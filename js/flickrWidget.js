$(document).ready(() => {    
    let img = $('.flickr').find('img');
    let modal = img.siblings('.modal');
    let span = modal.find(".close");
    let modalImg = modal.find('.modal-content');
    let caption = modal.find('.caption');

    img.on('click', (e) => {
        modal.css('display', 'block');
        modalImg.attr('src', $(e.target).attr('src'));
        caption.text($(e.target).attr('alt'));
    });

    span.on('click', () => { 
        modal.css('display', 'none');
    });
});