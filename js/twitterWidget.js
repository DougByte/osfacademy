$(document).ready(() => {
    let tweetsContainer = $('.tweets');
    let tweets = [];
    let tweetIndex = 0;

    function updateTweets() {
        for(let i = 0; i < 2; i++) {
            let message = tweets[tweetIndex].msg;            
            let mentions = message.match(/@\w+/g);
            let tweet = $('<p></p>');
            let tweetDate = $('<p></p>').text(tweets[tweetIndex].date);

            for(let j = 0; j < mentions.length; j++){                
                mentions[j] = '<span class="mention">' + mentions[j] + '</span>';
                message = message.replace(/@\w+/, 'mentionplaceholder');    
            }
            
            for(let j = 0; j < mentions.length; j++){
                message = message.replace('mentionplaceholder', mentions[j]);
            }

            tweet.append(message);
            tweetsContainer.append(tweet);
            tweetsContainer.append(tweetDate);
            tweetsContainer.find('p').hide();
            tweetsContainer.find('p').delay(50).show(300);
            tweetIndex++;
            if(tweetIndex >= tweets.length)
                tweetIndex = 0;
        }
    }

    $.getJSON('../json/tweets.json', (data) => {
        tweets = data.tweets;    
        if(tweets.length > 0) {
            updateTweets();
            setInterval( () => {
                tweetsContainer.find('p').hide(300);
                tweetsContainer.delay(310).empty();
                updateTweets();
            }, 5000);
        }
    });
});