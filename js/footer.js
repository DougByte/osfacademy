$(document).foundation();
$(document).ready(() => {
    $('#saveEmail').on('click', () => {
        let email = {
            email: $('#email').val()
        };
        if(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email.email))
            localStorage.setItem('email', JSON.stringify(email));
    });
});