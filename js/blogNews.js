$(document).ready(() => {
    let blogNews = $('.blog').find('.card-section');
    let blogNewsPosts = [];
    let postIndex = 0;

    function updateNews() {
        for(let i = 0; i < 2; i++){
            let blogPost = $('<div class="blog-post flex-container"></div>');
            let postDate = $('<span class="date"></span>').text(blogNewsPosts[postIndex].date);
            let postContent = $('<div></div>');
            let postTitle = $('<h6></h6>').text(blogNewsPosts[postIndex].title);
            let postDescription = $('<p></p>').text(blogNewsPosts[postIndex].description);

            blogNews.append(blogPost.append(postDate).append(postContent.append(postTitle).append(postDescription)));
            blogNews.find('div').hide();
            blogNews.find('div').delay(50).show(300);
            postIndex++;
            if(postIndex >= blogNewsPosts.length)
                postIndex = 0;
        }
    }

    $.getJSON('../json/blogNews.json', (data) => {
        blogNewsPosts = data.blogPosts;        
        if(blogNewsPosts.length > 0) {
            updateNews();
            setInterval( () => {
                blogNews.find('div').hide(300);
                blogNews.find('div').delay(310).remove('.blog-post');
                updateNews();
            }, 5000);
        }
    });
});