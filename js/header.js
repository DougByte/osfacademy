$(document).foundation();
$(document).ready(() => {
    let search = $('.search').find('input');
    let loginDialog = $( ".login" );
    let registerDialog = $( ".register" );

    if (window.innerWidth < 960) {
        search.hide();

        search.focusout(() => {
            search.hide(100);
        });
    }

    fetch('../json/suggestions.json').then((response) => {
        if(response.ok) {
            response.json().then((data) => {
                search.autocomplete({source: data.categories});   
            });
        } else {
          console.log('Network response was not ok.');
        }
    }).catch((error) => {
        console.log('There has been a problem with your fetch operation: ' + error.message);
        let suggestions = {
            "categories": [
                "MEN SHOES",
                "KIDS"
            ]
        };

        search.autocomplete({source: suggestions.categories});   
    });

    $('.fa-search').parent().on('click', () => {
        search.show(100);
        search.focus();
    });

    loginDialog.resizable();
    
    loginDialog.dialog({ 
        autoOpen: false,
        resizable: false,
        draggable: false,
        buttons: [
            {
              text: "Sign in",              
              click: function() {
                $( this ).dialog( "close" );
              }
            }
          ]
     });
    
    $('.loginDialog').click(() => {
        loginDialog.dialog('open');
    });

    registerDialog.resizable();
    
    registerDialog.dialog({ 
        autoOpen: false,
        resizable: false,
        draggable: false,
        buttons: [
            {
              text: "Create account",              
              click: function() {
                $( this ).dialog( "close" );
              }
            }
          ]
     });
    
    
    $('.registerDialog').click(() => {
        registerDialog.dialog('open');
    });

});