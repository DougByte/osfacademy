$(document).ready(() => {
    let productTile = $('.product ');
    let productName = productTile.find('.product-name');
    let iconButtons = productTile.find('.icon-btns');    
    
    $('header').load('../html/header.html');
    $('footer').load('../html/footer.html');

    iconButtons.hide();

    productTile.on({
        mouseenter: function(){
            if(!productTile.parent().hasClass('list-showcase')) {
                $(this).find(productName).hide();
                $(this).find(iconButtons).show();
            }
        },
        mouseleave: function(){
            if(!productTile.parent().hasClass('list-showcase')) {
                $(this).find(iconButtons).hide();
                $(this).find(productName).show();
            }
        }
    });
});